<?php

class share {
  var $share_id;
  var $type;
  var $label;
  var $node_types = array();
  var $category_terms = array();
  var $where = 'links';
  var $view = 1;
  var $status = 1;

  function load_popup($share_id, $type) {
    switch ($type) {
      case 'share':
        $data = db_fetch_object(db_query("SELECT * FROM {share} AS s LEFT JOIN {share_share} AS s_share ON s.share_id = s_share.share_id WHERE s.share_id = %d", $share_id));
        break;
      case 'sharethis':
        $data = db_fetch_object(db_query("SELECT * FROM {share} AS s LEFT JOIN {share_sharethis} AS s_share ON s.share_id = s_share.share_id WHERE s.share_id = %d", $share_id));
        $data->tabs = variable_get('share_sharethis_tabs', array('web', 'post', 'email'));
        $data->services = variable_get('share_sharethis_services', array('reddit','digg','facebook','myspace','delicious','stumbleupon','technorati','google_bmarks','yahoo_bmarks','yahoo_myweb','windows_live','propeller','slashdot','newsvine','n4g','mixx','blinklist','furl','magnolia','mister_wong'));
        $data->publisher = variable_get('share_sharethis_publisher', 'd4a10265-492e-4646-82f0-bb513c7ca25f');
        $data->code = variable_get('share_sharethis_code', '');
        break;
    }

    foreach ($data as $key => $value) {
      switch ($key) {
        case 'node_types':
        case 'category_terms':
          $this->$key = unserialize($value);
          break;
        case 'offsetTop':
          $this->offset['top'] = ($value) ? $value : '';
          break;
        case 'offsetLeft':
          $this->offset['left'] = ($value) ? $value : '';
          break;
        default:
          $this->$key = $value;
          break;
      }
    }
  }
}

class share_share extends share {
  var $tabs = array();

  function defaults() {
    $this->type = 'share';
    $this->label = t('Share popup');
    $this->name = t('Share');
    $this->effects = 1;
    $this->tabs = $this->default_tabs();
  }

  function default_tabs() {
    $all_tabs = array();
    foreach (module_implements('share_info') as $module) {
      $func = $module .'_share_info';
      $tab = $func();

      $all_tabs[$tab['id']] = (object)$tab;
      $all_tabs[$tab['id']]->module = $module;
    }

    foreach ($this->tabs as $tab) {
      unset($all_tabs[$tab->id]);
    }

    $tabs = array();
    foreach ($all_tabs as $value) {
      $tabs[$value->weight][$value->id] = $value;
    }

    $enabled = array();
    foreach ($tabs as $tab) {
      ksort($tab);
      $enabled+= array_values($tab);
    }
    $tabs = array_merge($this->tabs, $enabled);
    return $tabs;
  }

  function load_tabs() {
    $result = db_query("SELECT * FROM {share_tabs} WHERE share_id = %d ORDER BY weight", $this->share_id);
    $tabs = array();
    while ($data = db_fetch_object($result)) {
      $tab = $data;
      $tab_data = (object)module_invoke($data->module, 'share_tab', 'load', $this);
      $tab = (array)$tab + (array)$tab_data;
      $tab = (object)$tab;
      $tabs[] = $tab;
      unset($all_tabs[$data->id]);
    }
    $this->tabs = $tabs;
  }

  function render($node, $op = FALSE) {
    $output = '';
    switch ($op) {
      case 'link':
        $output.= l(t('Share'), 'share/'. $node->nid, array('id' => 'share-'. $this->share_id .'-'. $node->nid, 'class' => 'share-link'));
        break;
      case 'popup':
        $output.= $this->render_popup($node);
        break;
      default:
        $output = $this->render($node, 'link');
        $output.= $this->render($node, 'popup');
        break;
    }
    return $output;
  }

  function render_popup($node) {
    $tabs = array();
    $content = array();
    foreach ($this->tabs as $tab) {
      if (!$tab->enabled) {
        continue;
      }

      $tabs[] = array(
        'id' => str_replace('_', '-', check_plain($tab->id)),
        'name' => $tab->title
      );

      $func = $tab->module .'_share_tab';
      $result = $func('process', $tab, $node);
      $content[] = array(
        'id' => str_replace('_', '-', check_plain($tab->id)),
        'content' => $result
      );
    }
    $data['tabs'] = theme('share_popup_tabs', $tabs, $node->nid);
    $data['content'] = theme('share_popup_content', $content, $node->nid);
    return theme('share_popup', $data);
  }
}

class share_sharethis extends share {
  function defaults() {
    $services = array(
      'reddit',
      'digg',
      'facebook',
      'myspace',
      'delicious',
      'stumbleupon',
      'technorati',
      'google_bmarks',
      'yahoo_bmarks',
      'yahoo_myweb',
      'windows_live',
      'propeller',
      'slashdot',
      'newsvine',
      'n4g',
      'mixx',
      'blinklist',
      'furl',
      'magnolia',
      'mister_wong'
    );

    $this->type = 'sharethis';
    $this->label = t('ShareThis popup');
    $this->name = t('ShareThis');
    $this->tabs = array('web', 'post', 'email');
    $this->services = $services;
    $this->icon;
    $this->publisher = 'd4a10265-492e-4646-82f0-bb513c7ca25f';
    $this->button = 1;
    $this->buttonText;
    $this->popup = 0;
    $this->offset = array(
      'top' => '',
      'left' => ''
    );
    $this->onclick;
    $this->code = '';
  }

  function gen_setting($setting) {
    $string = array();
    foreach ($this->$setting as $value) {
      if ($value) {
        $string[] = $value;
      }
    }
    $string = implode(',', $string);
    return $string;
  }

  function gen_head() {
    $settings = array();
    $settings[] = 'tabs='. drupal_urlencode($this->gen_setting('tabs'));
    $settings[] = 'services='. drupal_urlencode($this->gen_setting('services'));
    $settings[] = 'charset=utf-8';
    $settings[] = 'style=default';
    $settings[] = 'publisher='. $this->publisher;

    $settings = implode('&amp;', $settings);

    return '<script type="text/javascript" src="http://w.sharethis.com/widget/?'. $settings .'"></script>';
  }

  function page_props($node) {
    $path = (isset($node->nid)) ? 'node/'. $node->nid : $_GET['q'];
    $path = drupal_get_path_alias($path);
    $props = array(
      'title' => $node->title,
      'url' => url($path, NULL, NULL, TRUE),
      'summary' => $node->teaser,
      'content' => $node->body,
      'updated' => $node->updated,
      'published' => $node->created,
      'author' => $node->author
    );
    foreach ($props as $key => $prop) {
      if (!$prop) {
        unset($props[$key]);
      }
    }
    if ($this->icon) {
      $props['icon'] = $this->icon;
    }
    return drupal_to_js($props);
  }

  function advanced_settings() {
    $advanced = array(
      'button' => TRUE
    );
    if ($this->buttonText) {
      $advanced['buttonText'] = $this->buttonText;
    }
    if ($this->popup) {
      $advanced['popup'] = $this->popup;
    }
    if ($this->onclick) {
      $advanced['onclick'] = $this->onclick;
    }
    if ($this->offset['top']) {
      $advanced['offsetTop'] = $this->offset['top'];
    }
    if ($this->offset['left']) {
      $advanced['offsetLeft'] = $this->offset['left'];
    }
    return drupal_to_js($advanced);
  }

  function render($node) {
    static $sharethis = 0;

    if ($sharethis == 0) {
      if ($this->code) {
        drupal_set_html_head($this->code);
      } else {
        drupal_set_html_head($this->gen_head());
      }
      $sharethis++;
    }

    $output = '<script language="javascript" type="text/javascript">';
    $output.= 'SHARETHIS.addEntry('. $this->page_props($node) .', '. $this->advanced_settings() .')';
    $output.= '</script>';
    return $output;
  }
}
