<?php

/**
 * share_admin.inc@file
 * Functions for administering Share.
 *
 * This file holds the functions that build and theme the Share administration
 * pages. This is only loaded when the uri begins with 'admin/build/share'.
 *
 * Written by greenSkin. Development sponsored by the It Is Written.
 * http://www.itiswritten.org
 */

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

function share_admin_popups() {
  $result = db_query("SELECT * FROM {share}");
  $popups = array();
  while ($data = db_fetch_object($result)) {
    $popups[$data->type][] = $data;
  }

  if (empty($popups)) {
    return t('No Share popups have been created. Click !here to create one.', array('!here' => l(t('here'), 'admin/build/share/add')));
  }

  ksort($popups);
  return theme('share_list_popups', $popups);
}

function share_admin_popups_add($form_values = NULL) {
  $tabs = module_invoke_all('share_info');
  if (empty($tabs)) {
    drupal_goto('admin/build/share/add/sharethis');
  }

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Popup type'),
    '#description' => t('Select the type of popup.'),
    '#default_value' => 'share',
    '#options' => array('share' => t('Share'), 'sharethis' => t('Share this'))
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next')
  );

  return $form;
}

function share_admin_popups_add_submit($form_id, $form_values) {
  drupal_goto('admin/build/share/add/'. $form_values['type']);
}

function share_admin_popups_share_form($share_id = NULL) {
  $form['#validate']['share_admin_popups_form_validate'] = array('share');

  if ($share_id && is_numeric($share_id)) {
    $share = share_popup_load($share_id, 'share');
    $share->tabs = $share->default_tabs();

    $form['share_id'] = array(
      '#type' => 'hidden',
      '#value' => $share_id
    );
    $form['#submit']['share_admin_popups_form_update'] = array('share');
  } else {
    $share = new share_share;
    $share->defaults();

    $form['#submit']['share_admin_popups_form_insert'] = array('share');
  }

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable this popup'),
    '#default_value' => $share->status
  );
  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => 'share'
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Give a label for this popup.'),
    '#default_value' => $share->label,
    '#required' => TRUE
  );

  // WHERE
  $form['where'] = share_where_settings($share);

  $form['share']['link_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Link name'),
    '#description' => t('Specify the name of the Share link.'),
    '#default_value' => $share->name,
    '#required' => TRUE
  );
  $form['share']['effects'] = array(
    '#type' => 'hidden',
    '#title' => t("Effects"),
    '#description' => t("Add animation to the opening and closing of the Share popup."),
    '#default_value' => $share->effects
  );

  $form['tabs'] = array(
    '#theme' => 'share_tabs_settings'
  );
  foreach ($share->tabs as $key => $tab) {
    $form['tabs'][$key][$tab->id][$tab->id .'_id'] = array(
      '#type' => 'hidden',
      '#value' => $tab->id
    );
    $form['tabs'][$key][$tab->id][$tab->id .'_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => $tab->title
    );
    $form['tabs'][$key][$tab->id][$tab->id .'_enabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => $tab->enabled
    );
    $form['tabs'][$key][$tab->id][$tab->id .'_weight'] = array(
      '#type' => 'weight',
      '#delta' => 10,
      '#default_value' => $tab->weight
    );
    $form['tabs'][$key][$tab->id][$tab->id .'_module'] = array(
      '#type' => 'hidden',
      '#value' => $tab->module
    );

    $func = $tab->module .'_share_tab';
    if ($share_id) {
      $func('load', $tab);
    }
    if ($settings = $func('settings', $tab)) {
      $form['tabs'][$key][$tab->id][$tab->id .'_settings'] = $settings;
    }
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  if ($share_id) {
    $form['save_edit'] = array(
      '#type' => 'submit',
      '#value' => t('Save and edit')
    );
  }

  return $form;
}

function share_admin_popups_sharethis_form($share_id = NULL) {
  $form['#validate']['share_admin_popups_form_validate'] = array('sharethis');

  if ($share_id && is_numeric($share_id)) {
    $share = share_popup_load($share_id, 'sharethis');

    $form['share_id'] = array(
      '#type' => 'hidden',
      '#value' => $share_id
    );
    $form['#submit']['share_admin_popups_form_update'] = array('sharethis');
  } else {
    $share = new share_sharethis;
    $share->defaults();
    $form['#submit']['share_admin_popups_form_insert'] = array('sharethis');
  }

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable this popup'),
    '#default_value' => $share->status
  );
  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => 'sharethis'
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Give a label for this popup.'),
    '#default_value' => $share->label,
    '#required' => TRUE
  );

  // WHERE
  $form['where'] = share_where_settings($share);

  // GLOBAL
  $form['sharethis']['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global'),
    '#description' => t('These settings apply to all ShareThis popups.'),
    '#collapsible' => TRUE,
    '#collapsed' => (!variable_get('share_sharethis_services', FALSE)) ? FALSE : TRUE
  );
  $form['sharethis']['global']['popup']['#theme'] = 'sharethis_settings';
  $form['sharethis']['global']['popup']['tabs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose your tabs'),
    '#default_value' => $share->tabs,
    '#options' => array('web' => t('Social Web'), 'post' => t('Post'), 'email' => t('Send/Email')),
    '#required' => TRUE
  );

  $services = share_sharethis_services();
  $form['sharethis']['global']['popup']['services'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose Your Social Web Services'),
    '#default_value' => $share->services,
    '#options' => $services
  );
  $form['sharethis']['global']['popup']['services_order'] = array(
    '#type' => 'textarea',
    '#default_value' => implode(',', $share->services),
    '#prefix' => '<div style="display: none;">',
    '#suffix' => '</div>'
  );
  $form['sharethis']['global']['popup']['share'] = array(
    '#type' => 'value',
    '#value' => $share
  );
  $form['sharethis']['global']['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#description' => t('Only use these settings if you know what you are doing.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['sharethis']['global']['advanced']['publisher'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID'),
    '#description' => t('Specify your publisher id to track clicks for ShareThis.'),
    '#default_value' => $share->publisher
  );
  $form['sharethis']['global']['advanced']['code'] = array(
    '#type' => 'textarea',
    '#title' => t('ShareThis Code'),
    '#description' => t('If set, this takes precedence over other ShareThis settings and skips validation for those settings. Javascript code provided by ShareThis. You can generate your own code !here.', array('!here' => l(t('here'), 'http://sharethis.com/publisher?type=stapi'))),
    '#default_value' => $share->code
  );

  $form['sharethis']['icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon path'),
    '#description' => t('Specify a path for a custom icon to use.'),
    '#default_value' => $share->icon
  );
  $form['sharethis']['button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Button text'),
    '#default_value' => $share->buttonText
  );
  $form['sharethis']['popup'] = array(
    '#type' => 'checkbox',
    '#title' => t('Popup window'),
    '#description' => t('If set, the popup will show in a new window.'),
    '#default_value' => $share->popup
  );
  $form['sharethis']['offset']['top'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset top'),
    '#description' => t('Changes the vertical positioning of the ShareThis widget in relation to the button link.'),
    '#default_value' => $share->offset['top']
  );
  $form['sharethis']['offset']['left'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset left'),
    '#description' => t('Changes the horizontal positiong of the ShareThis widget in relation to the button link .'),
    '#default_value' => $share->offset['left']
  );
  $form['sharethis']['onclick'] = array(
    '#type' => 'textarea',
    '#title' => t('Code to execute on-click.'),
    '#description' => t('Allows for execution of a custom function when button is clicked.'),
    '#default_value' => $share->onclick
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  if ($share_id) {
    $form['save_edit'] = array(
      '#type' => 'submit',
      '#value' => t('Save and edit')
    );
  }

  return $form;
}

function share_admin_popups_status($state, $share_id) {
  switch ($state) {
    case 'disable':
      $status = 0;
      break;
    case 'enable':
      $status = 1;
      break;
  }
  db_query("UPDATE {share} SET `status` = %d WHERE `share_id` = %d", $status, $share_id);
  drupal_goto('admin/build/share');
}

function share_admin_popups_share_delete($share) {
  $form['share_id'] = array(
    '#type' => 'value',
    '#value' => $share->share_id
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $share->type
  );
  $form['label'] = array(
    '#type' => 'value',
    '#value' => $share->label
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete %title?', array('%title' => $share->label)),
    'admin/build/share',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/*******************************************************************************
 * Module and Helper Functions
 ******************************************************************************/

function share_admin_popups_form_validate($form_id, $form_values, $form, $type) {
  switch ($type) {
    case 'share':
      $set = FALSE;
      foreach ($form_values as $key => $value) {
        if (preg_match('/_enabled$/', $key)) {
          if ($value) {
            $set = TRUE;
            break;
          }
        }
      }
      if ($set == FALSE) {
        form_set_error('_enabled', t('At least one tab needs to be enabled.'));
        return;
      }
      foreach (module_implements('share_tab') as $module) {
        $func = $module .'_share_tab';
        $func('validate', $form_values);
      }
      break;
    case 'sharethis':
      if (empty($form_values['code'])) {
        $service_set = FALSE;
        foreach ($form_values['services'] as $service) {
          if ($service) {
            $service_set = TRUE;
          }
        }
        if (!$service_set && $form_values['tabs']['web']) {
          form_set_error('services', t('At least one service has to been selected.'));
        }
        if (!$form_values['publisher']) {
          form_set_value($form['sharethis']['advanced']['publisher'], 'd4a10265-492e-4646-82f0-bb513c7ca25f');
        }
      }
      break;
  }
}

function share_admin_popups_form_insert($form_id, $form_values, $type) {
  switch ($type) {
    case 'share':
      $share_id = share_popup_new('share', $form_values);
      drupal_set_message(t('@label created.', array('@label' => $form_values['label'])));
      break;
    case 'sharethis':
      $share_id = share_popup_new('sharethis', $form_values);
      drupal_set_message(t('@label created.', array('@label' => $form_values['label'])));
      break;
  }
  if ($form_values['op'] == $form_values['save']) {
    return 'admin/build/share';
  }
}

function share_admin_popups_form_update($form_id, $form_values, $type) {
  switch ($type) {
    case 'share':
      share_popup_save('share', $form_values);
      drupal_set_message(t('@label saved.', array('@label' => $form_values['label'])));
      break;
    case 'sharethis':
      share_popup_save('sharethis', $form_values);
      drupal_set_message(t('@label saved.', array('@label' => $form_values['label'])));
      break;
  }
  if ($form_values['op'] == $form_values['save']) {
    return 'admin/build/share';
  }
}

function share_admin_popups_share_delete_submit($form_id, $form_values) {
  share_popup_delete($form_values['share_id'], $form_values['type']);
  drupal_set_message(t('@label deleted.', array('@label' => $form_values['label'])));
  return 'admin/build/share';
}

function share_where_settings($share) {
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Where to share'),
    '#description' => t('Set node credentials for where you would like the popup to show.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $form['node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#description' => t('If no node type is selected, the popup will not be restricted by node type.'),
    '#default_value' => $share->node_types,
    '#options' => array_map('check_plain', node_get_types('names'))
  );

  if (module_exists('taxonomy')) {
    $terms = taxonomy_form_all();
    if (!empty($terms)) {
      $form['category_terms'] = array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => t('Categories'),
        '#description' => t('If no category term is selected, the popup will not be restricted by terms.'),
        '#default_value' => $share->category_terms,
        '#options' => taxonomy_form_all()
      );
    }
  } else {
    $form['category_terms'] = array(
      '#type' => 'hidden',
      '#value' => array()
    );
  }
  $form['where'] = array(
    '#type' => 'select',
    '#title' => t('Where'),
    '#default_value' => $share->where,
    '#options' => array('links' => t('Links'), 'nodes' => t('Nodes'))
  );
  $form['view'] = array(
    '#type' => 'select',
    '#title' => t('View'),
    '#default_value' => $share->view,
    '#options' => array(t('Teasers'), t('Full-page'), t('Teasers and full-page'))
  );

  return $form;
}

function share_sharethis_services() {
  $options = array(
    'reddit' => t('reddit'),
    'digg' => t('digg'),
    'facebook' => t('Facebook'),
    'myspace' => t('MySpace'),
    'delicious' => t('del.icio.us'),
    'stumbleupon' => t('StumbleUpon'),
    'technorati' => t('Technorati'),
    'google_bmarks' => t('Google Bookmarks'),
    'yahoo_bmarks' => t('Yahoo Bookmarks'),
    'yahoo_myweb' => t('Yahoo! My Web'),
    'windows_live' => t('Windows Live'),
    'propeller' => t('Propeller'),
    'slashdot' => t('Slashdot'),
    'newsvine' => t('newsvine'),
    'n4g' => t('N4G'),
    'mixx' => t('Mixx'),
    'blinklist' => t('blinklist'),
    'furl' => t('Furl'),
    'magnolia' => t('ma.gnolia'),
    'mister_wong' => t('Mister Wong'),
    'blogmarks' => t('BlogMarks'),
    'faves' => t('Faves'),
    'current%' => t('Current'),
    'simpy' => t('Simpy'),
    'meneame' => t('Meneame'),
    'yigg' => t('Yigg'),
    'oknotizie' => t('OKNOtizie'),
    'fresqui' => t('Fresqui'),
    'diigo' => t('Diigo'),
    'care2' => t('Care2'),
    'funp' => t('funP'),
    'kirtsy' => t('Kirtsy'),
    'hugg' => t('Hugg')
  );
  return $options;
}

/*******************************************************************************
 * Theme Functions
 ******************************************************************************/

function theme_share_list_popups($popups) {
  $headers = array(
    array('data' => t('Label')),
    array('data' => t('Node types')),
    array('data' => t('Actions'))
  );

  $output = '';
  foreach ($popups as $type => $value) {
    $rows = array();
    foreach ($value as $share) {
      $node_types = unserialize($share->node_types);
      $types = array();
      if (!empty($node_types)) {
        foreach ($node_types as $node_type) {
          if ($node_type) {
            $types[] = node_get_types('name', $node_type);
          }
        }
      }

      $actions = array(
        'delete' => array(
          'title' => t('Delete'),
          'href' => "admin/build/share/". $share->type ."/". $share->share_id ."/delete"
        ),
        'disable' => array(
          'title' => t('Disable'),
          'href' => "admin/build/share/". $share->share_id ."/disable"
        ),
        'enable' => array(
          'title' => t('Enable'),
          'href' => "admin/build/share/". $share->share_id ."/enable"
        ),
        'edit' => array(
          'title' => t('Edit'),
          'href' => "admin/build/share/". $share->type ."/". $share->share_id ."/edit"
        )
      );
      $row = array(
        array('data' => $share->label),
        array('data' => empty($types) ? '' : implode(', ', $types)),
        array(
          'data' => theme(
            'links',
            ($share->status) ? 
              array(
                $actions['edit'],
                $actions['disable']
              ) : array(
                $actions['edit'],
                $actions['enable'],
                $actions['delete']
              )
          )
        )
      );
      $rows[] = $row;
    }

    switch ($type) {
      case 'share':
        $share_type = t('Share');
        break;
      case 'sharethis':
        $share_type = t('ShareThis');
        break;
    }
    $element = array(
      '#title' => $share_type,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#value' => theme('table', $headers, $rows)
    );
    $output.= theme('fieldset', $element);
  }

  return $output;
}

function theme_share_tabs_settings($form) {
  drupal_add_js(drupal_get_path('module', 'share') .'/js/share_settings.js');
  drupal_add_css(drupal_get_path('module', 'share') .'/css/share_settings.css');

  $rows = array();
  $headers = array(
    array('data' => t('Tab')),
    array('data' => t('Weight'), 'width' => '67px')
  );

  foreach (element_children($form) as $weight) {
    foreach (element_children($form[$weight]) as $tab) {
      $tab_name = '<div class="tab-name"><div class="double-arrows"></div><div class="tab-weight">'. drupal_render($form[$weight][$tab][$tab .'_weight']) .'</div><div class="tab-enabled">'. drupal_render($form[$weight][$tab][$tab .'_enabled']) .'</div><div class="tab-title">'. $form[$weight][$tab][$tab .'_title']['#value'] .'</div></div>';
      $tab_settings = '<div class="tab-settings" style="display: none;">'. drupal_render($form[$weight][$tab][$tab .'_title']) . drupal_render($form[$weight][$tab][$tab .'_settings']) .'</div>';
      $tab = '<div id="'. str_replace('_', '-', $tab) .'-tab">'. $tab_name . $tab_settings .'</div>';

      $row = array(
        array('data' => $tab, 'colspan' => 2)
      );
      $rows[] = $row;
    }
  }
  $output = theme('table', $headers, $rows);
  $output.= drupal_render($form);
  return $output;
}

function theme_sharethis_settings($form) {
  drupal_add_js(drupal_get_path('module', 'share') .'/js/sharethis_settings.js');
  drupal_add_css(drupal_get_path('module', 'share') .'/css/sharethis_settings.css');

  $share = $form['share']['#value'];

  $output = '<div id="sharethis-settings">'. drupal_render($form['tabs']);
  $output.= '<div id="sharethis-settings-services">'. drupal_render($form['services']) .'</div>';
  $output.= drupal_render($form['services_order']);
  $output.= '<div style="clear: both;"></div></div>';
  // $output.= '<div id="sharethis-settings-view">'. $share->render((object)array('title' => t('Settings'))) .'</div>';
  $output.= '<div style="clear: both;">'. drupal_render($form) .'</div>';
  return $output;
}
