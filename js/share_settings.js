
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    shareOpenTab("div.tab-title");
    shareOpenTab("div.double-arrows");
  });
}

function shareOpenTab(name) {
  $(name).each(function(i) {
    var id = $(this).parent().parent().attr('id');
    $(this).click(function() {
      var image = $(this).parent();
      $("#" + id + " div.tab-settings").animate({
        height: 'toggle'
      },function() {
        $(image).toggleClass('opened');
      });
    });
  });
}
