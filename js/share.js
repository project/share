
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    $(".share-link").each(function(i) {
      var share = $(this).parent();
      var shareLink = share.children("a.share-link");
      var sharePopup = share.children("div.share-popup");
      var shareHeader = sharePopup.children("div.share-header");
      var shareMenu = shareHeader.children("ul.share-menu").find('a');
      var shareContent = sharePopup.children("div.share-content");

      // Open/Close Share popup
      shareLink.click(function() {
        $(sharePopup).animate({
          'height': 'toggle'
        }, 'fast');
        return false;
      });

      // Close Share popup
      shareHeader.children("a.share-close").click(function() {
        $(sharePopup).animate({
          'height': 'hide'
        }, 'fast');
        return false;
      });

      $.each(shareMenu, function(j, n) {
        $(this).click(function() {
          var tabContent = 'div.'+ $(this).parent().attr('class');
          if ($(tabContent).css('display') == 'none') {
            $.each(shareMenu, function(k, o) {
              var otherTabContent = 'div.'+ $(this).parent().attr('class');
              if ($(otherTabContent).css('display') != 'none') {
                $(otherTabContent).animate({
                  'height': 'hide',
                  'opacity': 'hide'
                });
                $(this).toggleClass('selected');
              }
            });
            $(this).toggleClass('selected');
            $(tabContent).animate({
              'height': 'show',
              'opacity': 'show'
            });
          }
          return false;
        });
      });
    });
  });
}
